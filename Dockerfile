FROM node:13.6.0-alpine3.11

RUN addgroup -g 501 ec2-user \
    && adduser -u 501 -G ec2-user -s /bin/sh -D ec2-user \
    && apk add git \
    && apk add bash \
    && apk add openssh-client \
    && npm i typescript ts-node -g \
    && npm i @feathersjs/cli -g \
    && npm i @angular/cli -g \
    && npm i firebase-tools -g
    
USER ec2-user
